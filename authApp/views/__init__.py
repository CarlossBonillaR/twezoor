from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .team_pgView import Team_pgView
from .notificationView import NotificationView
from .commentView import CommentView 
from .messageView import MessageView 