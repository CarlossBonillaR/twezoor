from rest_framework import serializers

from authApp.models import Team_pg
""""serializar mi objeto.  Convertir de Objetos(Diccionario o listas) a JSON"""
class Team_pgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team_pg
        fields = (
            'team_id',
            'user_id',
            'title',
            'isActive'
        )